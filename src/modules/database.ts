// import dependencies
import BetterSQLite from "better-sqlite3"

// initialize Database
export const Database = new BetterSQLite("main.db")
Database.exec(`
  CREATE TABLE IF NOT EXISTS applications;
  CREATE TABLE IF NOT EXISTS members;
`);

// prepare SQL Statements
export const GET_GUILDCONFIG = Database.prepare("SELECT * from guilds WHERE id = ?")
export const GET_ADMINGUILDCONFIG = Database.prepare("SELECT * from guilds WHERE isAdminGuild = true")

// export interfaces
export interface GuildConfig {
  readonly id: string
  readonly applyChannel: string
  readonly isAdminGuild: boolean
}