import DiscordLibrary from "discord.js"

export const DiscordClient = new DiscordLibrary.Client({ 
  intents: [
    DiscordLibrary.Intents.FLAGS.GUILDS,
    DiscordLibrary.Intents.FLAGS.GUILD_MEMBERS,
    DiscordLibrary.Intents.FLAGS.GUILD_BANS,
    DiscordLibrary.Intents.FLAGS.GUILD_WEBHOOKS,
    DiscordLibrary.Intents.FLAGS.GUILD_PRESENCES,
    DiscordLibrary.Intents.FLAGS.GUILD_MESSAGES,
    DiscordLibrary.Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
    DiscordLibrary.Intents.FLAGS.DIRECT_MESSAGES,
    DiscordLibrary.Intents.FLAGS.DIRECT_MESSAGE_REACTIONS
  ]
})