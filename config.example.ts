interface Authentication {
  readonly Discord: string
  readonly Roblox: string
}
interface DiscordConfig {
  readonly ManagementGuildId: string
  readonly AdminRoleId: string
  readonly AlertsChannelId: string
}
interface RobloxConfig {
  readonly GroupId: string
  readonly MMCPlaceId: string
}
interface DeploymentConfig {
  readonly Authentication: Authentication
  readonly DiscordConfig: DiscordConfig
  readonly RobloxConfig: RobloxConfig
}
interface Config {
  readonly Production: DeploymentConfig
  readonly Staging: DeploymentConfig
}

const Configuration: Config = {
Staging: {
  Authentication: {
    Discord: '',
    Roblox: '',
  },
  DiscordConfig: {
    ManagementGuildId: '',
    AdminRoleId: '',
    AlertsChannelId: '',
  },
  RobloxConfig: {
    GroupId: '',
    MMCPlaceId: '',
  },
},
Production: {
  Authentication: {
    Discord: '',
    Roblox: '',
  },
  DiscordConfig: {
    ManagementGuildId: '',
    AdminRoleId: '',
    AlertsChannelId: '',
  },
  RobloxConfig: {
    GroupId: '',
    MMCPlaceId: '',
  },
},
};

export default Configuration;
